import React, { useState, Fragment } from 'react';
import Header from './components/Header/Header.js';
import QuestionCreater from './components/QuestionCreater/QuestionCreater.js';
import QuestionsList from './components/QuestionsList/QuestionsList.js';
import './App.css';

let defaultQuestionInfo = {
  answersType: 'list',
  multiAnswers: false,
  answersInfo: {
      list: [
        {
          text: 'Закрытый вопрос',
          type: 'close',
        }
      ], 
      table: {
          columns: [
            {
              text: 'Столбец 1',
              type: 'close',
            }
          ],
          rows: ['Строка 1'],
      }
  },
  picture: '',
  name: 'Текст вопроса',
}

function App() {
  const [questionCreateFormVisible, setQuestionCreateFormVisible] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [editQuestion, setEditQuestion] = useState(false);
  const [questionInfo, setQuestionInfo] = useState(Object.assign({}, defaultQuestionInfo));

  function questionInfoHandler(questionKey, action) {
    let newQuestionInfo = Object.assign({}, questions[questionKey]);

    switch (action) {
      case 'edit':
          setEditQuestion(questionKey);
          setQuestionInfo(newQuestionInfo);
          setQuestionCreateFormVisible(true);
          break;
      case 'clone':
          setQuestionInfo(newQuestionInfo);
          setQuestionCreateFormVisible(true);
          break;
      case 'delete':
          let questionsNew = questions;
          questionsNew.splice(questionKey, 1);
          setQuestions(questionsNew);
          setQuestionDefault();
          break;
      default:
          return;
    }
  }

  function setQuestionDefault(options) {
    let questionInfo = Object.assign({}, defaultQuestionInfo);
    questionInfo.answersInfo = {
      list: [
        {
          text: 'Закрытый вопрос',
          type: 'close',
        }
      ], 
      table: {
          columns: [
            {
              text: 'Столбец 1',
              type: 'close',
            }
          ],
          rows: ['Строка 1'],
      }
    };
    if(options) {
      questionInfo = Object.assign(questionInfo, options);
    }
    setQuestionInfo(questionInfo);
  }

  function questionCreateFormVisibleHandler(state) {
    setQuestionCreateFormVisible(state);
    if(editQuestion !== false) {
      setEditQuestion(false);
    }
  }

  function updateQuestionsHandler(questionInfo, questionNumber, action) {
    switch (action) {
      case 'success':
        if(questionNumber > questions.count) {
          addQuestion(questionInfo);
        } else {
          updateQuestion(questionInfo, questionNumber);
        }
        break;
      default:
        return;
    }
  }

  function addQuestion(questionInfo) {
    let newQuestionsInfo = questions;
    newQuestionsInfo.push(questionInfo);
    setQuestions(newQuestionsInfo);
    setQuestionDefault();
  }

  function updateQuestion(updateQuestionInfo, questionNumber) {
    let newQuestionsInfo = questions;
    newQuestionsInfo[questionNumber - 1] = updateQuestionInfo;
    setQuestions(newQuestionsInfo);
    setQuestionDefault();
  }

  function updateQuestionInfoHandler(questionInfo) {
    setQuestionInfo(questionInfo);
  }

  function renderStartPage() {
    if(questions.length) {
      return (
        <Fragment>
          <QuestionsList
              questions={questions}
              questionInfoHandler={questionInfoHandler}
          />
          <QuestionCreater
              type='button'
              questionCount={questions.length}
              updateQuestionsHandler={updateQuestionsHandler} 
              updateQuestionInfoHandler={updateQuestionInfoHandler} 
              questionInfo={questionInfo}
              questionCreateFormVisible={questionCreateFormVisible}
              questionCreateFormVisibleHandler={questionCreateFormVisibleHandler}
              setQuestionDefault={setQuestionDefault}
              editQuestion={editQuestion}
          />
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <QuestionCreater
            questionCount={questions.length}
            updateQuestionsHandler={updateQuestionsHandler} 
            updateQuestionInfoHandler={updateQuestionInfoHandler} 
            questionInfo={questionInfo}
            questionCreateFormVisible={questionCreateFormVisible}
            questionCreateFormVisibleHandler={questionCreateFormVisibleHandler}
            setQuestionDefault={setQuestionDefault}
            editQuestion={editQuestion}
            />
          <div className="content__note">
            Вы не добавили ни одного вопроса для исследования. Выберите тип вопроса, в котором внесите интересующие вас настройки и добавьте варианты ответов.
          </div>
        </Fragment>
      );
    }
  }

  return (
    <Fragment>
      <Header 
        count={questions.length}
      />

      <div className="content">
        {renderStartPage()}
      </div>
    </Fragment>
  );
}

export default App;