import React, { useState } from 'react';
import './InterviewName.css';

function InterviewName() {
    const [name, setName] = useState('Default name');

    function getName() {
        let newName = prompt('Enter new name', name);
        if(newName) {
            setName(newName);
        }
    }

    return (
        <div 
            className="interview-name"
            onClick={getName}
        >
            {name}
        </div>
    );
}
  
export default InterviewName;