import React from 'react';
import './AnswerList.css';
import checkBoxEmpty from '../../images/checkBoxEmpty.png';
import radioBox from '../../images/radioBox.png';

function AnswerList(props) {
    return (
        <div className={props.editable ? "answer-creater" : "answer-creater answer-creater--public"}>
            <div className="answer-creater__answers">
                {renderAnswers()}
            </div>
            {renderActions()}
        </div>
    );

    function renderAnswers() {
        if(props.questionInfo.answersType === 'list') {
            return renderListAnswers(props.questionInfo.answersInfo[props.questionInfo.answersType]);
        } else if(props.questionInfo.answersType === 'table') {
            return renderTableAnswers(props.questionInfo.answersInfo[props.questionInfo.answersType]);
        }
    }

    function renderListAnswers(listAnswers) {
        let answersCount = listAnswers.length;
        if(answersCount) {
            let answers = [];
            listAnswers.forEach( (answer, answerKey) => {
                answers.push(
                    renderListAnswer(answer, answerKey)
                );
            });

            return answers;
        }
    }

    function renderListAnswer(answer, answerKey) {
        let elementKey = answerKey + 1;
        if(props.editable) {
            return (
                <div 
                    key={elementKey}
                    className={'answer-creater__answer answer-creater__answer--' + answer.type}
                    onClick={changeAnswerText}
                    number={answerKey}
                >
                    <div className="answer-creater__answer-counter"> {props.questionNumber + '.' + elementKey + '.'} </div>
                    {renderInput('', answerKey, answer.text)}
                    <div 
                        className='answer-creater__answer-deleter'
                        onClick={deleteAnswer}
                    >x</div>
                </div>
            );
        } else {
            if(answer.type === 'open') {
                let name = 'answer_' + props.questionNumber + '_' + answerKey + '_open';
                return (
                    <div 
                        key={elementKey}
                        className={'answer-creater__answer answer-creater__answer--' + answer.type}
                        number={answerKey}
                    >
                        <div className="answer-creater__answer-counter"> {props.questionNumber + '.' + elementKey + '.'} </div>
                        {renderInput('', answerKey)}
                        <input 
                            type="text"
                            name={name}
                            placeholder={answer.text}
                            />
                    </div>
                );
            } else {
                return (
                    <div 
                        key={elementKey}
                        className={'answer-creater__answer answer-creater__answer--' + answer.type}
                        number={answerKey}
                    >
                        <div className="answer-creater__answer-counter"> {props.questionNumber + '.' + elementKey + '.'} </div>
                        {renderInput('', answerKey, answer.text)}
                    </div>
                );
            }
        }
    }

    function renderTableAnswers(tableAnswers) {
        let columnsCount = tableAnswers.columns.length;
        if(columnsCount) {
            return (
                <table className="answer-creater__table" width="100%" cellPadding="10" border="1" cellSpacing="0">
                    <thead>
                        <tr>
                            {renderTableColumns(tableAnswers.columns)}
                        </tr>
                    </thead>
                    <tbody>
                        {renderTableRows(tableAnswers.rows, tableAnswers.columns)}
                    </tbody>
                </table>
            );
        }
    }

    function renderActions() {
        if( !props.editable )
            return false;

        if(props.questionInfo.answersType === 'list') {
            return (
                <div className="answer-creater__actions">
                    <div 
                        className="answer-creater__create answer-creater__create--close"
                        type="close"
                        onClick={createAnswer}
                    >Закрытый</div>
                    <div 
                        className="answer-creater__create answer-creater__create--open"
                        type="open"
                        onClick={createAnswer}
                    >Открытый</div>
                    <div 
                        className="answer-creater__create answer-creater__create--special"
                        type="special"
                        onClick={createAnswer}
                    >Специальный</div>
                </div>
            );
        }  else if(props.questionInfo.answersType === 'table') {
            return (
                <div className="answer-creater__actions">
                    <div className="answer-creater__create-wrapper--top">
                        <div 
                            className="answer-creater__create answer-creater__create--row"
                            onClick={createRow}
                        >Добавить строку</div>
                    </div>
                    <div className="answer-creater__create-wrapper--right">
                        <div 
                            className="answer-creater__create answer-creater__create--close"
                            type="close"
                            onClick={function() {createTableAnswer('close')}}
                        >Закрытый</div>
                        <div 
                            className="answer-creater__create answer-creater__create--special"
                            type="special"
                            onClick={function() {createTableAnswer('special')}}
                        >Специальный</div>
                    </div>
                </div>
            );
        }
    }

    function setColumnText(columnKey) {
        if( !props.editable )
            return false;

        let newAnswersInfo = props.questionInfo.answersInfo;
        
        let newText = prompt('Enter new text', newAnswersInfo[props.questionInfo.answersType].columns[columnKey].text);
        if(newText) {
            newAnswersInfo[props.questionInfo.answersType].columns[columnKey].text = newText;
            props.answersInfoHandle(newAnswersInfo);
        }
    }

    function setRowText(rowKey) {
        if( !props.editable )
            return false;

        let newAnswersInfo = props.questionInfo.answersInfo;

        let newText = prompt('Enter new text', newAnswersInfo[props.questionInfo.answersType].rows[rowKey]);
        if(newText) {
            newAnswersInfo[props.questionInfo.answersType].rows[rowKey] = newText;
            props.answersInfoHandle(newAnswersInfo);
        }
    }
    
    function changeAnswerText(e) {
        if( !props.editable )
            return false;
            
        let label = '';
        let target = e.target;

        target = target.closest('.answer-creater__answer');
        label = target.querySelector('.answer-creater__answer-label');
        let labelText = label.textContent.trim();
        let number = target.getAttribute('number');
        
        let newText = prompt('Enter new text', labelText);
        if(newText) {
            let newAnswersInfo = props.questionInfo.answersInfo;
            newAnswersInfo[props.questionInfo.answersType][number].text = newText;

            props.answersInfoHandle(newAnswersInfo);
        }
    }

    function renderTableColumns(columns) {
        let result = [];
        result.push(<th key={-1}>&nbsp;</th>);
        columns.forEach( (column, columnKey) => {
            result.push(
                <th 
                    number={columnKey} 
                    key={columnKey}
                    onClick={function() {setColumnText(columnKey)}}
                    className={column.type}
                >
                    {column.text}
                </th>
            );
        });
        return result;
    }

    function renderTableRows(rows, columns) {
        let result = [];
        rows.forEach( (row, rowKey) => {
            result.push(
                <tr 
                    number={rowKey} 
                    key={rowKey}
                >
                    {renderTableRow(row, rowKey, columns)}
                </tr>
            );
        });
        return result;
    }

    function renderTableRow(row, rowKey, columns) {
        let result = [];
        result.push(
            <td 
                key={-1}
                onClick={() => setRowText(rowKey)}
                >
                {row}
            </td>
        );
        columns.forEach( (column, columnKey) => {
            result.push(
                <td key={columnKey} className="empty-td">
                    {renderInput(rowKey, columnKey)}
                </td>
            );
        });
        return result;
    }

    function deleteAnswer(e) {
        let target = e.target.closest('.answer-creater__answer');
        let number = target.getAttribute('number');

        let newAnswersInfo = props.questionInfo.answersInfo;
        newAnswersInfo[props.questionInfo.answersType].splice(number, 1);
        props.answersInfoHandle(newAnswersInfo);

        e.stopPropagation();
    }

    function createAnswer(e) {
        let answerType = e.target.getAttribute('type');
        let currentText = getDefaultText(answerType);

        const answerInfo = {
            text: currentText,
            type: answerType,
        };
        let newAnswersInfo = props.questionInfo.answersInfo;
        newAnswersInfo[props.questionInfo.answersType].push(answerInfo);

        newAnswersInfo[props.questionInfo.answersType].sort(function(a, b) {
            if(a.type !== b.type) {
                switch (a.type) {
                    case 'special':
                        return 1;
                    case 'open':
                        if(b.type === 'special')
                            return -1;
                        else
                            return 1;
                    default:
                        return -1;
                }
            } else {
                return 0;
            }
        });

        props.answersInfoHandle(newAnswersInfo);
    }

    function createTableAnswer(answerType) {
        let newAnswersInfo = props.questionInfo.answersInfo;
        let newCount = newAnswersInfo[props.questionInfo.answersType].columns.length + 1;
        let newColumn = {
            text: answerType === 'close' ? 'Столбец ' + newCount : 'Специальный ответ',
            type: answerType,
        }
        newAnswersInfo[props.questionInfo.answersType].columns.push(newColumn);

        newAnswersInfo[props.questionInfo.answersType].columns.sort(function(a, b) {
            return (
                    a.type === 'special' ? 1 : 
                    (
                        b.type === 'special' ? -1 : 0
                    )
                );
        });

        props.answersInfoHandle(newAnswersInfo);
    }

    function createRow() {
        let newAnswersInfo = props.questionInfo.answersInfo;
        let newCount = newAnswersInfo[props.questionInfo.answersType].rows.length + 1;
        newAnswersInfo[props.questionInfo.answersType].rows.push('Строка ' + newCount);

        props.answersInfoHandle(newAnswersInfo);
    }

    function renderInput(name, value, labelText) {
        let prefix = 'answer_' + props.questionNumber + (name !== '' ? '_' + name : '');
        let id = prefix + '_' + value;
        let label = '';
        if(labelText) {
            label = <label 
                className="answer-creater__answer-label" 
                htmlFor={id}
                >
                    {labelText}
            </label>;
        }

        if( props.editable ) {
            return (
                <div className="answer-creater__answer-input">
                    <img 
                        src={props.questionInfo.multiAnswers ? checkBoxEmpty : radioBox}
                        alt={props.questionInfo.multiAnswers ? "Чекбокс" : "Радиобокс" }
                        title={props.questionInfo.multiAnswers ? "Чекбокс" : "Радиобокс" } 
                    />
                    {label}
                </div>
            );
        } else {
            if(props.questionInfo.multiAnswers) {
                return (
                    <div className="answer-creater__answer-input">
                        <input 
                            type='checkBox' 
                            id={id}
                            name={prefix + '_' + value}
                            />
                        {label}
                    </div>
                );
            } else {
                return (
                    <div className="answer-creater__answer-input">
                        <input 
                            type='radio'
                            id={id}
                            name={prefix}
                            value={value}
                            />
                        {label}
                    </div>
                );
            }
            
        }
    }

    function getDefaultText(type) {
        let currentText = '';
        switch (type) {
            case 'close':
                currentText = 'Закрытый вариант ответа';
                break;
            case 'open':
                currentText = 'Открытый вариант ответа';
                break;
            case 'special':
                currentText = 'Специальный вариант ответа';
                break;
            default:
                break;
        }
        return currentText;
    }
}
  
export default AnswerList;