import React from 'react';
import './FileSaver.css';

function FileSaver(props) {
    function changeFileHandler(e) {
        let target = e.target;
        let file = target.files[0];
        if(file.type && file.type.indexOf('image') !== -1) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
    
            reader.onload = function() {
                let parent = target.parentNode;
                parent.style['background-image'] = 'url('+reader.result+')';
                props.onSave(reader.result);
            };
        } else {
            alert('Загружен некорретный файл!');
        }
        
    }

    function openFileDialog(e) {
        let target = e.target;
        if( !target.classList.contains('file-saver') ) {
            target = target.parentNode;
        }
        let childs = target.children;
        if(childs.length) {
            for(let child of childs) {
                if( child.classList.contains('file-saver__input') ) {
                    child.click();
                    return;
                }
            }
        }
    }

    return (
        <div 
            className="file-saver"
            onClick={openFileDialog}
        >
            <span className="file-saver__text">Загрузить изображение вопроса</span>
            <input 
                type="file" 
                className="file-saver__input" 
                onChange={changeFileHandler}
            />
        </div>
    );
}
  
export default FileSaver;