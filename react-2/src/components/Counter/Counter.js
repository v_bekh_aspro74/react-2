import React from 'react';
import './Counter.css';

function Counter(props) {
    return (
        <div className="counter">
            {props.count}
        </div>
    );
}
  
export default Counter;