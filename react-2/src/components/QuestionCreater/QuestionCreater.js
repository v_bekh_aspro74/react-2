import React, { Fragment } from 'react';
import './QuestionCreater.css';
import listImage from '../../images/list.png';
import tableImage from '../../images/table.png';

import QuestionCreateForm from '../QuestionCreateForm/QuestionCreateForm.js';

function QuestionCreater(props) {
    return (
        <Fragment>
            <div className={props.type === 'button' ? 'question-creater--button' : 'question-creater'}>
                {renderButtons()}
            </div>

            <QuestionCreateForm
                type='hiden'
                visible={props.questionCreateFormVisible}
                visibleChangeHandler={visibleChange}
                questionInfo={props.questionInfo}
                updateQuestionInfo={updateQuestionInfo}
                saveHandler={addQuestion}
                questionCount={props.questionCount}
                editQuestion={props.editQuestion}
            />
        </Fragment>
    );

    function renderButtons() {
        if(props.type === 'button') {
            return (
                <div 
                    className="question-creater__button--plus"
                    onClick={function() {openQuestionCreateForm('list')}}
                >+</div>
            );
        } else {
            return (
                <Fragment>
                    <div 
                        className="question-creater__button"
                        onClick={function() {openQuestionCreateForm('list')}}
                    >
                        <img 
                            src={listImage} 
                            alt="Список" 
                            title="Список" 
                            className="question-creater__button-img" 
                        />
                        <div className="question-creater__button-title">
                            Список
                        </div>
                    </div>

                    <div 
                        className="question-creater__button"
                        onClick={function() {openQuestionCreateForm('table')}}
                    >
                        <img 
                            src={tableImage} 
                            alt="Табличный" 
                            title="Табличный" 
                            className="question-creater__button-img" 
                        />
                        <div className="question-creater__button-title">
                            Табличный
                        </div>
                    </div>
                </Fragment>
            );
        }
    }

    function updateQuestionInfo(questionInfo) {
        props.updateQuestionInfoHandler(questionInfo);
    }

    function openQuestionCreateForm(questionType) {
        props.setQuestionDefault({answersType: questionType});
        props.questionCreateFormVisibleHandler(true);
    }

    function visibleChange() {
        props.questionCreateFormVisibleHandler(!props.questionCreateFormVisible);
    }

    function addQuestion(questionNumber) {
        props.updateQuestionsHandler(props.questionInfo, questionNumber, 'success');
        visibleChange();
    }
}
  
export default QuestionCreater;