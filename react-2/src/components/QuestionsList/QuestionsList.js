import React from 'react';
import $ from "jquery";
import './QuestionsList.css';
import AnswerList from '../AnswerList/AnswerList.js';
import answerArrow from '../../images/answerArrow.png';
import actionBar from '../../images/actionBar.png';

function QuestionsList(props) {
    return (
        <div className="questions-list">
            {renderQuestions()}
        </div>
    );

    function renderQuestions() {
        let questions = [];
        props.questions.forEach( (questionInfo, questionKey) => {
            questions.push(renderQuestion(questionInfo, questionKey + 1));
        });

        return questions;
    }

    function renderQuestion(questionInfo, questionKey) {
        return (
            <div 
                className="questions-list__item"
                key={questionKey}
                number={questionKey}
            >
                <div className="questions-list__item-info">
                    <div 
                        className="questions-list__item-arrow"
                        onClick={toggleAnswers}
                    >
                        <img 
                            src={answerArrow}
                            alt="Скрыть/раскрыть ответы" 
                            title="Скрыть/раскрыть ответы" 
                            className="questions-list__item-arrow-img" 
                        />
                    </div>

                    <div className="questions-list__item-number">{questionKey}.</div>
                    <div className="questions-list__item-text">{questionInfo.name}</div>

                    <div className="questions-list__item-action-bar-wrapper">
                        <img 
                            src={actionBar}
                            alt="Скрыть/раскрыть меню" 
                            title="Скрыть/раскрыть меню" 
                            className="questions-list__item-action-bar-img" 
                        />

                        <div className="questions-list__item-action-bar">
                            <div className="questions-list__item-action-bar-inner">
                                <div 
                                    className="questions-list__item-action-bar-item"
                                    onClick={() => changeQuestions(questionKey, 'edit')}
                                    >Редактировать</div>
                                <div 
                                    className="questions-list__item-action-bar-item"
                                    onClick={() => changeQuestions(questionKey, 'clone')}
                                    >Дублировать</div>
                                <div 
                                    className="questions-list__item-action-bar-item"
                                    onClick={() => changeQuestions(questionKey, 'delete')}
                                    >Удалить</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div 
                    className="questions-list__answers-wrapper"
                    style={{display: 'none'}}
                    >
                    <AnswerList
                        editable={false}
                        questionInfo={questionInfo}
                        questionNumber={questionKey}
                    />
                </div>            
            </div>
        );
    }

    function toggleAnswers(e) {
        let question = e.target.closest('.questions-list__item');
        let answers = question.querySelector('.questions-list__answers-wrapper');
        let arrow = question.querySelector('.questions-list__item-arrow');
        $(answers).slideToggle();
        arrow.classList.toggle('opened');
    }

    function changeQuestions(questionNumber, action) {
        props.questionInfoHandler(questionNumber - 1, action);
    }
}
  
export default QuestionsList;