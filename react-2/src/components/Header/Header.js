import React from 'react';
import './Header.css';
import InterviewName from '../InterviewName/InterviewName.js';
import Counter from '../Counter/Counter.js';

function Header(props) {
    return (
        <header className="header">
            <InterviewName />
            <Counter 
                count={props.count}
            />
        </header>
    );
}
  
export default Header;