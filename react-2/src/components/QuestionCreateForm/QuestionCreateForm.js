import React, { Fragment }  from 'react';
import './QuestionCreateForm.css';
import AnswerList from '../AnswerList/AnswerList.js';
import FileSaver from '../FileSaver/FileSaver.js';
import currentImage from '../../images/current.png';
import listImage from '../../images/list.png';
import tableImage from '../../images/table.png';
import checkBox from '../../images/checkBox.png';
import checkBoxEmpty from '../../images/checkBoxEmpty.png';

function QuestionCreateForm(props) {
    let questionNumber = props.editQuestion === false ? props.questionCount + 1 : props.editQuestion + 1;
    return (
        <Fragment>
            <div className={getClassName()}>
                <div className="question-create-form__part question-create-form__part--top">
                    <div className="question-create-form__part question-create-form__part--left">
                        <div    
                            className="question-create-form__answer-changer"
                            onClick={openAnswerTypeChanger}
                        >
                            <img 
                                src={props.questionInfo.answersType === 'list' ? listImage : tableImage} 
                                alt="Сменить тип ответов" 
                                title="Сменить тип ответов" 
                                className="question-create-form__answer-changer-img" 
                            />
                        </div>
                        <div className="question-create-form__number">{questionNumber}.</div>
                        <textarea 
                            className="question-create-form__question-name" 
                            value={props.questionInfo.name}
                            onChange={(e) => updateQuestion( {name: e.target.value} )}
                        ></textarea>
                    </div>
                    <div className="question-create-form__part question-create-form__part--right">
                        <div 
                            className="question-create-form__close"
                            onClick={props.visibleChangeHandler}
                        >X</div>
                        <div 
                            className="question-create-form__save"
                            onClick={() => props.saveHandler(questionNumber)}
                        >Готово</div>
                    </div>
                </div>

                <div className="question-create-form__main-part">
                    <FileSaver
                        onSave={onSaveFileHandler}
                    />

                    <AnswerList
                        editable={true}
                        questionInfo={props.questionInfo}
                        answersInfoHandle={answersInfoHandle}
                        questionNumber={questionNumber}
                    />

                    {renderAnswerTypeChangePanel()}
                </div>

                <div className="question-create-form__bottom-part">
                    <div className="question-create-form__bottom-title">
                        Дополнительные настройки вопроса:
                    </div>
                    <div className="question-create-form__bottom-options">
                        <div 
                            className="question-create-form__bottom-option"
                            onClick={() => updateQuestion( {multiAnswers: !props.questionInfo.multiAnswers} )}
                        >
                            <img 
                                src={props.questionInfo.multiAnswers ? checkBox : checkBoxEmpty}
                                alt="Разрешить выбор нескольких вариантов" 
                                title="Разрешить выбор нескольких вариантов" 
                                className="question-create-form__bottom-option-img" 
                            />
                            <div className="question-create-form__bottom-option-name">
                                Разрешить выбор нескольких вариантов
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div 
                className="overlay"
                onClick={props.visibleChangeHandler}
            ></div>
        </Fragment>
    );

    function updateQuestion(newInfo) {
        let newQuestionInfo = Object.assign({}, props.questionInfo);
        newQuestionInfo = Object.assign(newQuestionInfo, newInfo);
        props.updateQuestionInfo(newQuestionInfo);
    }

    function answersInfoHandle(answersInfo) {
        updateQuestion( {answersInfo: answersInfo} );
    }

    function onSaveFileHandler(file) {
        updateQuestion( {picture: file} );
    }

    function changeAnswerType(newAnswerType) {
        if(newAnswerType === props.questionInfo.answersType)
            return;

        updateQuestion( {answersType: newAnswerType} );
        closeAnswerTypeChanger();
    }

    function renderAnswerTypeChangePanel() {
        return (
            <div className="side-menu">
                <div 
                    className={'side-menu__item' + (props.questionInfo.answersType === 'list' ? ' side-menu__item--current' : '')}
                    onClick={() => changeAnswerType('list')}
                    >
                    <div className="side-menu__item-left-part">
                        <img 
                            src={props.questionInfo.answersType === 'list' ? currentImage : listImage} 
                            alt="Список" 
                            title="Список" 
                            className="side-menu__item-img" 
                        />
                    </div>
                    <div className="side-menu__item-right-part">
                        <div className="side-menu__item-name">Список</div>
                        <div className="side-menu__item-descr">Ответ из предложенных вариантов</div>
                    </div>
                </div>
                <div 
                    className={'side-menu__item' + (props.questionInfo.answersType === 'table' ? ' side-menu__item--current' : '')}
                    onClick={() => changeAnswerType('table')}
                    >
                    <div className="side-menu__item-left-part">
                        <img 
                            src={props.questionInfo.answersType === 'table' ? currentImage : tableImage}
                            alt="Табличный" 
                            title="Табличный" 
                            className="side-menu__item-img" 
                        />
                    </div>
                    <div className="side-menu__item-right-part">
                        <div className="side-menu__item-name">Табличный</div>
                        <div className="side-menu__item-descr">Множественные вопросы и ответы</div>
                    </div>
                </div>
            </div>
        );
    }

    function closeAnswerTypeChanger() {
        let menu = document.querySelector('.side-menu.active');
        menu.classList.remove('active');
    }

    function openAnswerTypeChanger(e) {
        let parent = e.target.closest('.question-create-form');
        let menu = parent.querySelector('.side-menu');
        menu.classList.add('active');
    }

    function getClassName() {
        let formClassName = 'question-create-form';
        formClassName += props.visible ? ' question-create-form--visible' : '';

        return formClassName;
    }
}
  
export default QuestionCreateForm;